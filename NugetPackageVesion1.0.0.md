# Creating a Nuget Package

As part of a learning project, I needed to find a way to make a reusable
component that could be used for the rest of our developers as a
starting point on future projects. My first thought was simple to commit
the could I make this a Nuget package that was privately hosted in our
infrastructure but for this time I create a public nuget package
manager.

**Nusepc file format**

First we need to understand the format of
a [.nuspec](https://docs.microsoft.com/en-au/nuget/reference/nuspec) file.
This is the main component of a Nuget package.

![](Image/media/image1.png)

The **id** must be unique if the package is to be hosted on Nuget.org
but that’s not the aim of our project, so make it what you like.

The **version** is crucial to how Visual Studio Nuget Package Manager
will interpret updates to the package. Start with 1.0.0 and increment in
the [usual](https://semver.org/) major/minor/patch structure. Once a
package is installed in a project, Package Manager will keep track of
changes and show an upgrade icon if it detects a new version available
on the Nuget server (local or remote).

The **title** and **authors** fields are obvious, though with authors
you can match these to existing Nuget.org authors to form a link that
can be navigated.

The **dependencies** group lists other Nuget packages you want to be
automatically installed before your package. This is powerful in that
you can define specific versions that will work with your project or
allow it to get the latest version in the repository. You need to enter
the exact name and version as shown on Nuget.org for a match to be
found.

The files list is the most important aspect of a Nuget package. You can
leave it out entirely and let the package automatically add everything,
but this may have side-effects so it’s best to be explicit.

File nodes have a **src** parameter and optional **target** parameter.
The **src** is where the file/folder resides in your file system; most
likely in a Visual Studio project. The **target** refers to where the
file will be copied to during creation of the Nuget Package itself.
There are specific folders used for package creation that determine how
they will be handled during installation of the package. In my example
above you can see most are going into a **content** folder. Content
folders are where static files will be copied to and obey the same
hierarchy as defined in your local folder structure. If I wanted to
include a DLL library, I would use the target of **lib. **Packages are
framework-agnostic, which is why they don’t just assume binaries live in
Bin like a .NET project.

You can see some variations in the example above.

  - **txt -** If you include this file, it will automatically be opened
    by Visual Studio once the package has completed installation. Use
    this for information about your package that can’t be communicated
    in the summary

  - **Content files –** Regular files that you want installed into the
    destination project via the package such as .cshtml, .cs, .css, .js
    etc. are content files. You can define that all files in a folder
    are to be included by specifying with two asterisk i.e. **\*\***

  - **Transforms** – you can easily transform web and app config files
    during installation of the package. The examples above include
    properties that need to be added to the assumed existing configs
    found at the **src** Pay attention to the naming convention of the
    files, and the standard transform attributes below. Example:

> **Creating a Nuget Package:**

1.  **At first, we need to download nuget.exe in project folder for
    create this command.**

2.  **Then we need to edit package.nuspec in visual studio**

![](Image/media/image2.png)

3.  Then we need to use under this command for create nuget package.

> ![](Image/media/image3.png)

4.  Finally, we can upload this **nuget package** as like:

> ![](Image/media/image4.png)
